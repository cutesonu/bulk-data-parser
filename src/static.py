EMPTY = ""
SPACE = ' '

# [annotation keywords]
ANNO_TEXT = 'text'
ANNO_BBOX = 'boundingBox'
ANNO_VERTICES = 'vertices'


# [page keywords]
PAGE_ID = "page_id"
PAGE_CONTENT = "page_content"
PAGE_ROTATE = "page_rotate"
PAGE_POSITION = "position"
PAGE_BBOX = "page_bbox"
PAGE_ANNOS = "page_annos"
PAGE_LINES = "page_lines"


# [line keywords]
LINE_ANNO_IDS = "line_anno_ids"
LINE_POS = "pos"
LINE_TEXT = 'line_text'
