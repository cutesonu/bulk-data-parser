import os


# root
_cur_dir = os.path.dirname(os.path.realpath(__file__))
ROOT_DIR = os.path.abspath(os.path.join(_cur_dir, os.pardir))
print(ROOT_DIR)

# paths
UPLOAD_DIR = os.path.join(ROOT_DIR, "data")
LOG_DIR = os.path.join(ROOT_DIR, "logs")
INFO_DIR = os.path.join(ROOT_DIR, "dataset")

# api key
API_KEY = ''


INPUT_EXT = ".pdf"
INFO_EXT = ".json"


MACHINE = "local"

try:
    from src.settings_local import *
except Exception as e:
    print(e)
    pass
