import copy
import math
from src.static import ANNO_BBOX, ANNO_TEXT, ANNO_VERTICES


class AnnoProperty:
    def __init__(self):
        """
            annotation = {
                u'text': u'Description',
                u'boundingBox': {
                    u'vertices': [{u'x': 13, u'y': 8}, {u'x': 13, u'y': 18}, {u'x': 31, u'y': 18}, {u'x': 31, u'y': 8}]
                    }
            }
        """
        self.empty_anno = {
            u'text': '',
            u'boundingBox': {
                u'vertices': [
                    {u'x': -1, u'y': -1},
                    {u'x': -1, u'y': -1},
                    {u'x': -1, u'y': -1},
                    {u'x': -1, u'y': -1}]
            }
        }

    @staticmethod
    def get_height(anno):
        _ul, _ur, _br, _bl = anno[ANNO_BBOX][ANNO_VERTICES]
        return (_bl['y'] - _ul['y'] + _br['y'] - _ur['y']) / 2

    @staticmethod
    def get_width(anno):
        _ul, _ur, _br, _bl = anno[ANNO_BBOX][ANNO_VERTICES]
        return (_ur['x'] - _ul['x'] + _br['x'] - _bl['x']) / 2

    def get_font_sz(self, anno):
        return self.get_height(anno=anno)

    @staticmethod
    def get_cen_pt(anno):
        _ul1, _ur1, _br1, _bl1 = anno[ANNO_BBOX][ANNO_VERTICES]
        return {'x': (_ur1['x'] + _ul1['x'] + _br1['x'] + _bl1['x']) / 4,
                'y': (_ur1['y'] + _ul1['y'] + _br1['y'] + _bl1['y']) / 4}

    @staticmethod
    def get_text(anno):
        return anno[ANNO_TEXT]

    def merge_annos(self, annos):
        merged_anno = copy.deepcopy(self.empty_anno)
        new_text = ''
        li_x = []
        li_y = []
        for anno_id, anno in enumerate(annos):
            new_text += anno[ANNO_TEXT]
            for pt in anno[ANNO_BBOX][ANNO_VERTICES]:
                li_x.append(pt['x'])
                li_y.append(pt['y'])

        x1, x2 = min(li_x), max(li_x)
        y1, y2 = min(li_y), max(li_y)
        merged_anno[ANNO_TEXT] = new_text
        merged_anno[ANNO_BBOX][ANNO_VERTICES] = [{u'x': x1, u'y': y1}, {u'x': x2, u'y': y1}, {u'x': x2, u'y': y2},
                                                 {u'x': x2, u'y': y2}]
        return merged_anno

    def calc_dis_anno2anno(self, anno1, anno2):
        cen_pt1 = self.get_cen_pt(anno1)
        cen_pt2 = self.get_cen_pt(anno2)
        return math.sqrt((cen_pt1['x'] - cen_pt2['x']) ** 2 + (cen_pt1['y'] - cen_pt2['y']) ** 2)

    @staticmethod
    def calc_space_anno2anno(anno1, anno2):
        ul_1, ur_1, br_1, bl_1 = anno1[ANNO_BBOX][ANNO_VERTICES]
        ul_2, ur_2, br_2, bl_2 = anno2[ANNO_BBOX][ANNO_VERTICES]
        space_x = min(math.fabs(ul_1['x'] - ur_2['x']), math.fabs(ur_1['x'] - ul_2['x']))
        space_y = min(math.fabs(bl_1['x'] - ul_2['x']), math.fabs(ul_1['x'] - bl_2['x']))
        return {'horizon': space_x, 'vertical': space_y}

    @staticmethod
    def get_left_edge(anno):
        _ul, _ur, _br, _bl = anno[ANNO_BBOX][ANNO_VERTICES]
        return {'x': (_ul['x'] + _bl['x']) / 2,
                'y': (_ul['y'] + _bl['y']) / 2}

    @staticmethod
    def get_bottom_edge(anno):
        _ul, _ur, _br, _bl = anno[ANNO_BBOX][ANNO_VERTICES]
        return {'x': (_bl['x'] + _br['x']) / 2,
                'y': (_bl['y'] + _br['y']) / 2}

    @staticmethod
    def get_top_edge(anno):
        _ul, _ur, _br, _bl = anno[ANNO_BBOX][ANNO_VERTICES]
        return {'x': (_ul['x'] + _ur['x']) / 2,
                'y': (_ul['y'] + _ur['y']) / 2}

    @staticmethod
    def get_right_edge(anno):
        _ul, _ur, _br, _bl = anno[ANNO_BBOX][ANNO_VERTICES]
        return {'x': (_ur['x'] + _br['x']) / 2,
                'y': (_ur['y'] + _br['y']) / 2}
