import logging
import sys
import os
from src.settings import LOG_DIR


if not os.path.isdir(LOG_DIR):
    os.mkdir(LOG_DIR)

log_fn = os.path.join(LOG_DIR, 'bulk-data-parser.log')
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(message)s',
                    datefmt='%d/%b/%Y %H:%M:%S',
                    filename=log_fn)


def init():
    if os.path.isfile(log_fn):
        os.remove(log_fn)


def log_msg(msg):
    if msg[0] == '\r':
        msg = msg[1:]
    return msg


def log_info(msg):
    sys.stdout.write(msg + '\n')
    logging.info(log_msg(msg))


def log_warn(msg):
    sys.stdout.write(msg + '\n')
    logging.info(log_msg(msg))


def log_error(msg):
    sys.stdout.write(msg + '\n')
    logging.info(log_msg(msg))
