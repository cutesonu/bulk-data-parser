import cv2
import pytesseract as pt


class TesseractUtils:
    def __init__(self, debug=False):
        """
            '-l eng'    for using the English language
            '--oem 1'   for using the LSTM OCR engine
        """
        self.debug = debug
        self.config = '-l eng --oem 1 --psm 3'

    def detect_text(self, cv_im):
        text = pt.image_to_string(image=cv_im, config=self.config)
        return text

    def detect_text_annotation(self, cv_im):
        # detect the text from image
        str_boxes = pt.image_to_boxes(cv_im, config=self.config)

        # standardize
        sz = cv_im.shape[:2]
        annos = self.__standardize_anno_dict(str_boxes=str_boxes, im_sz=sz)

        # show the detected rects and texts
        if self.debug:
            for anno in annos:
                text = anno['text']
                print(text)
                pt1 = anno['boundingBox']['vertices'][0]
                pt2 = anno['boundingBox']['vertices'][2]
                cv2.rectangle(cv_img, (pt1['x'], pt1['y']), (pt2['x'], pt2['y']), (255, 0, 0), 1)
                cv2.putText(cv_img, text, (pt1['x'], pt1['y']), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
            cv2.imwrite("tesseract_text_boxes.jpg", cv_im)
        return annos

    def __standardize_anno_dict(self, str_boxes, im_sz):
        """
        annotation =
            {
                u'text': u'Description',
                u'boundingBox': {
                    u'vertices': [
                            {u'y': 13, u'x': 8},
                            {u'y': 13, u'x': 18},
                            {u'y': 31, u'x': 18},
                            {u'y': 31, u'x': 8}]
                }
            }
        """
        h, w = im_sz
        annos = []
        for str_line in str_boxes.splitlines():
            try:
                _char, _x1, _y1, _x2, _y2, _ = str_line.split(' ')
                _x1, _y1, _x2, _y2 = int(_x1), h - int(_y1), int(_x2), h - int(_y2)
                x1, x2 = min(_x1, _x2), max(_x1, _x2)
                y1, y2 = min(_y1, _y2), max(_y1, _y2)
                annos.append({
                    u'text': _char,
                    u'boundingBox': {
                        u'vertices': [{u'y': y1, u'x': x1},
                                      {u'y': y1, u'x': x2},
                                      {u'y': y2, u'x': x2},
                                      {u'y': y2, u'x': x1}]
                    }
                })
            except Exception as e:
                if self.debug:
                    print(e)
                else:
                    continue

        return annos


if __name__ == '__main__':
    from src.settings import ROOT_DIR
    import os
    tess = TesseractUtils(debug=False)
    img_path = os.path.join(ROOT_DIR, "data/samples/SSC1394201-Company Credit Report-1.jpg")
    cv_img = cv2.imread(img_path)
    img_patha = tess.detect_text_annotation(cv_im=cv_img)
    print("done")

    # print(tu.detect_text(cv_im=cv_img))
    # print(image_to_string(Image.open(img_path), lang='eng'))
