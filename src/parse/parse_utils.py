import json
from src.common.text_utils import find_keyword, clear_value


# [rule/sections info keys]
SUB_SECTIONS = "sub_sections"
FIELD_NAME = "field_name"
COMMON_FIELDS = "common_fields"
FIELDS = "fields"
SUB_FIELD = "sub_fields"
KEYWORDS = "keywords"

SECTION_NAME = "section_name"

MULTILINE = "multi-line"
SPLIT = "splitter"
ORIENTATION = "orientation"

#
IN_SAME_GROUP = 200

APPENDER = " | "


def load_info(info_path):
    try:
        with open(info_path, 'r') as jp:
            info = json.load(jp)
            return info
    except Exception as e:
        print(e)
        return None


def remove_double_space(text):
    double_space = "  "
    while text.find(double_space) != -1:
        text = text.replace(double_space, " ")
    return text


def get_obj_value(line_text, keywords, sub_field=False):
    value = None
    find_pos = -1
    for keyword in keywords:
        find_pos = find_keyword(line_text=line_text, keyword=keyword)
        if find_pos != -1:
            if find_pos < len(keyword) or sub_field:
                value = clear_value(text=line_text[find_pos + len(keyword):])
            break
    return find_pos, value


def print_data(data, depth=0):
    if isinstance(data, dict):
        for key in data.keys():
            print("\t" * depth, key, ":")
            val = print_data(data[key], depth=depth + 1)
            if val is not None:
                print("\t" * (depth + 1), val)
    elif isinstance(data, list):
        for ele in data:
            print_data(data=ele, depth=depth + 1)
    else:
        return data
