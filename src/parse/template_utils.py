import os
from src.settings import INFO_EXT, INFO_DIR
from src.static import LINE_TEXT, PAGE_ANNOS, PAGE_LINES
from src.parse.parse_utils import load_info, print_data, find_keyword, get_obj_value
import src.logger as log


# template info keys
TEMPLATE_NAME = "template_name"
FILE_NAME = "file_name"
RULE_INFO = "rule_info"
FOLDER_NAME = "folder_name"
SECTIONS = "sections"
KEYWORDS = "keywords"


class TemplateUtils:
    def __init__(self):
        self.template_infos = self.load_templates()

    @staticmethod
    def load_templates():
        template_infos = []
        templates_dir = os.path.join(INFO_DIR, "templates")
        paths = [os.path.join(templates_dir, fn) for fn in os.listdir(templates_dir)]
        for path in paths:
            base, ext = os.path.splitext(os.path.split(path)[1])

            template_info = load_info(info_path=path)
            if template_info is None:
                log.log_warn("no exist info for template {}".format(base))

            template_infos.append(template_info)
        return template_infos

    @staticmethod
    def load_rule_info(template_info):
        template_name = template_info[TEMPLATE_NAME]

        rule_info_dir = os.path.join(INFO_DIR, "rules", template_info[RULE_INFO][FOLDER_NAME])
        ordered_section_names = template_info[RULE_INFO][SECTIONS]

        sections = []
        for i, section_name in enumerate(ordered_section_names):
            section_info_path = os.path.join(rule_info_dir, "{}_{}.json".format(i, section_name))
            section_info = load_info(info_path=section_info_path)
            sections.append(section_info)

        return template_name, sections

    def check_template_id(self, first_page):
        lines = first_page[PAGE_LINES]
        for line in lines:
            for template_id, template_info in enumerate(self.template_infos):
                if get_obj_value(line_text=line[LINE_TEXT], keywords=template_info[KEYWORDS])[0] != -1:
                    return template_id
        return -1

    # [MAIN]
    def proc(self, page_contents):
        template_id = self.check_template_id(first_page=page_contents[0])
        template_info = self.template_infos[template_id]

        template_name, sections = self.load_rule_info(template_info=template_info)
        print("template name:", template_name)

        if template_name.lower() == "cedar_rose":
            from src.templates.cedar_rose.cedar_rose import CedarRose
            parser = CedarRose(sections=sections)
            data = parser.parse(page_contents=page_contents)

            print("\nResult:")
            print_data(data=data)
            return data
        else:
            log.log_warn("unknown template")
            return None
