import difflib
import math
import re
from fuzzywuzzy import fuzz


diff = difflib.Differ()
EMPTY = ''
SPACE = ' '


def is_empty_string(text):
    return len(text.replace(SPACE, EMPTY)) == 0


def is_with_digits(text):
    text = text.replace(' ', '')
    digits = re.findall('\d+', text)
    if len(digits) != 0:
        return True
    else:
        return False


def similarity_word(dst_str, src_str):
    ratio = float(fuzz.token_set_ratio(dst_str, src_str)) / 100.0
    avg_len = (len(dst_str) + len(src_str)) / 2
    sub_len = math.fabs(len(dst_str) - len(src_str)) / 2
    return ratio * (avg_len - sub_len) / avg_len


def equal(str1, str2):
    str1 = str1.upper().replace(" ", "")
    str2 = str2.upper().replace(" ", "")
    return similarity_word(str1, str2) > 0.9


def find_keyword(line_text, keyword):
    if len(line_text) <= len(keyword):
        ratio = float(fuzz.token_sort_ratio(line_text, keyword)) / 100.0
        max_ratio = ratio * len(line_text) / len(keyword)
        last_same_pos = 0
    else:
        max_ratio = 0
        last_same_pos = 0
        for pos in range(len(line_text) - len(keyword)):
            sub_text = line_text[pos: pos + len(keyword)]
            ratio = float(fuzz.token_sort_ratio(sub_text, keyword)) / 100.0  # instead of (fuzz.token_set_ratio)
            if max_ratio < ratio:
                max_ratio = ratio
                last_same_pos = pos

    if max_ratio > 0.9:
        return last_same_pos
    else:
        return -1


def clear_value(text):
    text = re.sub('[]:[]', EMPTY, text)
    return text


def get_digits(word):
    word = word.replace(' ', '')
    tmp = word
    tmp = tmp.replace(',', '0')
    tmp = tmp.replace('.', '0')
    digits = re.findall('\d+', tmp)
    if len(digits) == 0:
        return EMPTY

    max_di = ""
    for di in digits:
        if len(di) > len(max_di):
            max_di = di

    pos = tmp.find(max_di)
    if pos != -1:
        return word[pos: pos+len(max_di)]
    else:
        return EMPTY
