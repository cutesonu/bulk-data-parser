# [START import]

import os
import cv2
import sys
import numpy as np

from src.settings import INPUT_EXT
import src.logger as log
from src.pdf.pdf_to_img import Pdf2Img
from src.pdf.extract_data import Pdf2Data
from src.pdf.parse_xml import ParseXml
from src.parse.template_utils import TemplateUtils
from src.annotation.anno_utils import bundle_to_lines
from src.static import ANNO_TEXT, PAGE_ANNOS, PAGE_LINES, PAGE_CONTENT, PAGE_ID, PAGE_BBOX
# [END import]


# [START utils]
pdf2img = Pdf2Img()
pdf2data = Pdf2Data()
xml2anno = ParseXml()


#
class BulkDataParser:
    def __init__(self, debug=False):
        self.debug = debug

    @staticmethod
    def show_page_contents(page_img_paths, page_contents):
        for page in page_contents:
            page_id = page[PAGE_ID]
            content = page[PAGE_CONTENT]
            page_annos = page[PAGE_ANNOS]

            print("\t  page {}".format(page_id + 1))
            print("\t  annos:".format(page_id))
            for anno in page_annos:
                print("\t\t" + anno[ANNO_TEXT])

            cv_im = cv2.imread(page_img_paths[page_id], cv2.IMREAD_COLOR)
            _, _, p_w, p_h = page[PAGE_BBOX]
            im_h, im_w = cv_im.shape[:2]
            for textbox in content:
                for textline in textbox['textline']:
                    text = textline['text']
                    bbox = textline['@bbox']

                    [x1, y1, x2, y2] = (np.array([float(val) for val in bbox.split(",")]) * np.array(
                        [im_w / p_w, im_h / p_h, im_w / p_w, im_h / p_h])).astype(np.uint)
                    cv2.rectangle(cv_im, (int(x1), int(im_h - y2)), (int(x2), int(im_h - y1)), (255, 0, 0), 1)
                    cv2.putText(cv_im, text, (int(x1), int(im_h - y2)),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)

            cv2.imwrite("annotations_{}.jpg".format(page_id), cv_im)
            cv2.imshow("img", cv2.resize(cv_im, None, fx=0.5, fy=0.5))
            cv2.waitKey(0)

    @staticmethod
    def save_page_contents(page_contents, save_path):
        import json
        with open(save_path, 'w') as jp:
            json.dump({'temp': page_contents}, jp)

    @staticmethod
    def load_page_contents(load_path):
        import json
        with open(load_path, 'r') as jp:
            page_contents = json.load(jp)
        return page_contents['temp']

    #
    #
    #
    #
    #
    #
    def proc(self, document_path):
        log.log_info("document: {}".format(document_path))

        # check the existence of the document =================================================================
        if not os.path.exists(document_path):
            log.log_error("no exist. exit(1)")
            sys.exit(1)

        # check the extension =================================================================================
        base, ext = os.path.splitext(os.path.split(document_path)[1])
        log.log_info("base: {}, ext: {}".format(base, ext))
        if ext.lower() not in INPUT_EXT:
            log.log_error("not supported file format. exit(1)")
            sys.exit(1)

        # convert pdf to images ===============================================================================
        log.log_info("convert pdf to images")
        page_img_paths = pdf2img.pdf_to_imgs(pdf_path=document_path)
        log.log_info("\t# pages: {}".format(len(page_img_paths)))

        # extract data from pdf ===============================================================================
        log.log_info("extract data from pdf")
        raw_xml_string = pdf2data.pdf_to_anno_data(pdf_path=document_path)

        # sections raw to standardized json ====================================================================
        log.log_info("convert text data to json dict")
        pages = xml2anno.parse_xml2anno(raw_xml_string=raw_xml_string)

        # show the result annotations =========================================================================
        self.save_page_contents(page_contents=pages, save_path="AEC1124207-Company Credit Report.json")
        if self.debug:
            self.show_page_contents(page_img_paths=page_img_paths, page_contents=pages)

        # [restart with temp]

    def temp(self, base):
        folder = "../data/samples/"
        page_img_paths = [os.path.join(folder, fn) for fn in os.listdir(folder)
                          if os.path.splitext(fn)[1] == '.jpg' and os.path.splitext(fn)[0].find(base) != -1]
        page_img_paths.sort()

        pages = self.load_page_contents(load_path=base + ".json")
        # self.show_page_contents(page_img_paths=page_img_paths, page_contents=pages)

        for page in pages:
            page_annos = page[PAGE_ANNOS]
            page[PAGE_LINES] = bundle_to_lines(origin_annos=page_annos)

        temp_utile = TemplateUtils()
        temp_utile.proc(page_contents=pages)


if __name__ == '__main__':
    bdp = BulkDataParser(debug=True)
    log.log_info(">>>")
    # bdp.proc(document_path="../data/samples/SSC1394201-Company Credit Report.pdf")
    # bdp.proc(document_path="../data/samples/AEC1124207-Company Credit Report.pdf")

    base = ["SSC1394201-Company Credit Report", "AEC1124207-Company Credit Report"]
    BulkDataParser().temp(base[0])
