from copy import deepcopy
from src.templates.cedar_rose.sections.overview import Overview
from src.templates.cedar_rose.sections.address import Address
from src.templates.cedar_rose.sections.management import Management
from src.templates.cedar_rose.sections.profile import Profile
from src.templates.cedar_rose.sections.shareholders import Shareholders
from src.templates.cedar_rose.sections.activities import Activities
from src.templates.cedar_rose.sections.financials import Financials
from src.templates.cedar_rose.sections.references import References


from src.static import PAGE_LINES, LINE_TEXT
from src.parse.parse_utils import *
import src.logger as log


class CedarRose:
    def __init__(self, sections):
        self.sections = sections

    @staticmethod
    def check_section_name(section_name, keyword):
        return section_name.lower().find(keyword) != -1

    def choose_section_parser(self, section_name, section_info):
        if section_info is None:
            return None
        parser = None
        if self.check_section_name(section_name=section_name, keyword="overview"):
            parser = Overview(info=section_info)
        elif self.check_section_name(section_name=section_name, keyword="address"):
            parser = Address(info=section_info)
        elif self.check_section_name(section_name=section_name, keyword="management"):
            parser = Management(info=section_info)
        elif self.check_section_name(section_name=section_name, keyword="profile"):
            parser = Profile(info=section_info)
        elif self.check_section_name(section_name=section_name, keyword="shareholders"):
            parser = Shareholders(info=section_info)
        elif self.check_section_name(section_name=section_name, keyword="activities"):
            parser = Activities(info=section_info)
        elif self.check_section_name(section_name=section_name, keyword="financial"):
            parser = Financials(info=section_info)
        elif self.check_section_name(section_name=section_name, keyword="reference"):
            parser = References(info=section_info)
        else:
            log.log_warn("\tunknown sections type.\n")
        return parser

    @staticmethod
    def extract_data_from_sub_filed(field_name, sub_field_info, value_text):
        key_value_list = []
        sub_value = remove_double_space(value_text)

        for field in sub_field_info:
            sps = sub_value.split(field[SPLIT])
            sub_value = ""
            for i in range(1, len(sps)):
                sub_value += sps[i]
            key_value_list.extend([sps[0], field[FIELD_NAME]])

        key_value_list.insert(0, field_name)
        key_value_list.append(sub_value)

        data = []
        for i in range(0, len(key_value_list), 2):
            data.append({key_value_list[i]: key_value_list[i + 1]})
        return data

    def common_extract_data_from_section(self, info, lines):
        section_data = []

        # collect all keywords
        total_keywords = []

        # === collect [TOTAL] keyword of sub_sections and common_fields ================================================
        if SUB_SECTIONS in info.keys() and info[SUB_SECTIONS] != []:
            for field in info[COMMON_FIELDS]:
                total_keywords.extend(field[KEYWORDS])
            for sub_section in info[SUB_SECTIONS]:
                total_keywords.extend(sub_section[KEYWORDS])
                if FIELDS in sub_section.keys():
                    for field in sub_section[FIELDS]:
                        total_keywords.extend(field[KEYWORDS])
        else:
            for field in info[COMMON_FIELDS]:
                total_keywords.extend(field[KEYWORDS])

        # ================ scan the lines ==============================================================================
        last_line_id = 0
        cur_id = 0
        cur_sub_section = None
        cur_scan_fields = []

        while cur_id < len(lines):
            line_text = lines[cur_id][LINE_TEXT]

            # --- check start of new sub_section or not
            is_start_new_sub_section = False
            # find sub_section
            if SUB_SECTIONS in info.keys():
                for sub_section in info[SUB_SECTIONS]:
                    pos, val = get_obj_value(line_text=line_text, keywords=sub_section[KEYWORDS])
                    if pos < len(sub_section[KEYWORDS][0]) // 3 and val:
                        cur_sub_section = deepcopy(sub_section)
                        is_start_new_sub_section = True
                        break

            # --- determine the dst_to_add to append the parsed result ---------------------------------
            if is_start_new_sub_section:
                section_data.append({cur_sub_section[SECTION_NAME]: []})
                dst_to_add = section_data[-1][cur_sub_section[SECTION_NAME]]
            else:
                if cur_sub_section is not None:
                    if len(section_data) > 0 and cur_sub_section[SECTION_NAME] in section_data[-1].keys():
                        dst_to_add = section_data[-1][cur_sub_section[SECTION_NAME]]
                    else:
                        section_data.append({cur_sub_section[SECTION_NAME]: []})
                        dst_to_add = section_data[-1][cur_sub_section[SECTION_NAME]]
                else:
                    dst_to_add = section_data

            # --- fields of cur_sub_section + common files ---------------------------------------------
            if is_start_new_sub_section or (not is_start_new_sub_section and cur_sub_section is None):
                cur_scan_fields = deepcopy(info[COMMON_FIELDS])
                if cur_sub_section is not None and FIELDS in cur_sub_section.keys():
                    cur_scan_fields.extend(cur_sub_section[FIELDS])

            # --- parse the key/value on the line ------------------------------------------------------
            for field in cur_scan_fields:
                find_pos, value = get_obj_value(line_text=line_text, keywords=field[KEYWORDS])

                if value is None:
                    continue

                key = field[FIELD_NAME]
                # ----------------------------------------------------------------------------------
                # multi-line
                if (MULTILINE in field.keys() and field[MULTILINE] is True) or \
                        (ORIENTATION in field.keys() and field[ORIENTATION] == "under"):
                    _is_with_keyword_pos = -1
                    ext_multi_line_cnt = 0
                    _sub_vals = []

                    # go through the next lines ----------------------------------
                    while _is_with_keyword_pos == -1 and (cur_id + ext_multi_line_cnt + 1) < len(lines):
                        ext_next_line = lines[cur_id + ext_multi_line_cnt + 1]
                        _is_with_keyword_pos, _ = get_obj_value(line_text=ext_next_line[LINE_TEXT],
                                                                keywords=total_keywords)

                        # only no keyword lines ----------------------------------
                        if _is_with_keyword_pos == -1:
                            multi_line_txt = ext_next_line[LINE_TEXT]

                            if SUB_FIELD in field.keys():
                                # split the multi-line sub_field's splitter
                                sub_field_data = self.extract_data_from_sub_filed(field_name=field[FIELD_NAME],
                                                                                  value_text=multi_line_txt,
                                                                                  sub_field_info=field[SUB_FIELD])
                                _sub_vals.extend(sub_field_data)
                            else:
                                # append the line itself without split
                                _sub_vals.append(ext_next_line[LINE_TEXT])
                            ext_multi_line_cnt += 1

                    # standardize the multi-line result --------------------------
                    if isinstance(_sub_vals, list) and len(_sub_vals) > 0:
                        if isinstance(_sub_vals[0], dict):
                            dst_to_add.append({key: _sub_vals})
                        else:
                            _str_sub_val = _sub_vals[0]
                            for j in range(1, len(_sub_vals)):
                                _str_sub_val = APPENDER + _sub_vals[j]
                            dst_to_add.append({key: _str_sub_val})

                    cur_id += ext_multi_line_cnt

                # ----------------------------------------------------------------------------------
                # single sub-field
                elif SUB_FIELD in field.keys():
                        sub_field_data = self.extract_data_from_sub_filed(field_name=field[FIELD_NAME],
                                                                          value_text=value,
                                                                          sub_field_info=field[SUB_FIELD])
                        dst_to_add.extend(sub_field_data)

                # ----------------------------------------------------------------------------------
                # single key/value
                else:
                    dst_to_add.append({key: value})

                last_line_id = cur_id + 1
                break
            # for tar_field

            cur_id += 1
        # while lines

        return section_data, last_line_id

    def parse(self, page_contents):
        data = []

        lines = []
        for page in page_contents:
            lines.extend(page[PAGE_LINES])

        start_line_id = 0
        for cur_sec_id, section_info in enumerate(self.sections):
            section_name = section_info[SECTION_NAME]

            # if section_name != "REFERENCES":
            #     print("continue: {}".format(section_name))
            #     continue
            # start_line_id = 62

            parser = self.choose_section_parser(section_name=section_name, section_info=section_info)
            if parser is None:
                continue

            if cur_sec_id == len(self.sections) - 1:
                last_line_id = -1
            else:
                last_line_id = -1
                for line_id in range(start_line_id, len(lines)):
                    line = lines[line_id]
                    if get_obj_value(line_text=line[LINE_TEXT],
                                     keywords=self.sections[cur_sec_id + 1][KEYWORDS])[0] != -1:
                        last_line_id = line_id
                        break

            print(section_name, start_line_id, last_line_id)

            section_data, last_detected_line_id = \
                self.common_extract_data_from_section(info=section_info, lines=lines[start_line_id: last_line_id])
            print(section_data)
            data.append({section_name: section_data})
            if len(section_data) != 0:
                start_line_id = last_line_id  # last_detected_line_id + start_line_id

        return data
