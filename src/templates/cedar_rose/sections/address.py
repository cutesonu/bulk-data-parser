from src.parse.parse_utils import *
from copy import deepcopy
from src.annotation.anno_utils import LINE_TEXT


class Address:
    def __init__(self, info):
        self.info = info

    def extract_data(self, lines, last_line_id):
        section_data = []
        sub_section_data = []
        exist_flag = True

        # check the start line id with sections keywords
        cur_sub_section = None
        for cur_id in range(last_line_id, len(lines)):

            if exist_flag and cur_id - last_line_id > IN_SAME_GROUP:
                break

            line_text = lines[cur_id][LINE_TEXT]
            # check new sub_section start
            is_start_new_sub_section = False
            for sub_section in self.info[SUB_SECTIONS]:
                pos, val = get_obj_value(line_text=line_text, keywords=sub_section[KEYWORDS])
                if pos < len(sub_section[KEYWORDS][0]) and val:
                    is_start_new_sub_section = True
                    cur_sub_section = deepcopy(sub_section)
                    break
            if is_start_new_sub_section:
                last_line_id += 1
                section_data.append({
                    cur_sub_section[FIELD_NAME]: []
                })
                sub_section_data.clear()
                exist_flag = True
                continue

            for field in self.info[FIELDS]:
                find_pos, value = get_obj_value(line_text=line_text, keywords=field[KEYWORDS])
                if value is not None:
                    key = field[FIELD_NAME]
                    section_data[-1][cur_sub_section[FIELD_NAME]].append({key: value})
                    if value is not EMPTY:
                        last_line_id = cur_id + 1
                        exist_flag = True
                    break

        return section_data, last_line_id
