from src.parse.parse_utils import *
from copy import deepcopy
from src.annotation.anno_utils import LINE_TEXT


class Management:
    def __init__(self, info):
        self.info = info

    def extract_data(self, lines, last_line_id):
        section_data = []
        sub_section_data = []
        exist_flag = False

        # check the start line id with sections keywords
        cur_sub_section = None
        for cur_id in range(last_line_id, len(lines)):

            if exist_flag and cur_id - last_line_id > IN_SAME_GROUP:
                break

            line_text = lines[cur_id][LINE_TEXT]

            # check new sub_section start
            is_start_new_sub_section = False
            for sub_section in self.info[SUB_SECTIONS]:
                pos, val = get_obj_value(line_text=line_text, keywords=sub_section[KEYWORDS])
                if pos < len(sub_section[KEYWORDS][0]) and val:
                    is_start_new_sub_section = True
                    cur_sub_section = deepcopy(sub_section)
                    break

            # new sub sections then go to
            if is_start_new_sub_section:
                section_data.append({
                    cur_sub_section[FIELD_NAME]: []
                })
                sub_section_data.clear()
                exist_flag = True

            for field in self.info[FIELDS]:
                find_pos, value = get_obj_value(line_text=line_text, keywords=field[KEYWORDS])
                if value is not None:
                    if SUB_FIELD in field.keys():
                        sub_find_pos, sub_value = get_obj_value(line_text=value, keywords=field[SUB_FIELD][KEYWORDS],
                                                                sub_field=True)
                        if sub_value is not None:
                            section_data[-1][cur_sub_section[FIELD_NAME]].append(
                                {field[FIELD_NAME]: value[:sub_find_pos]})
                            section_data[-1][cur_sub_section[FIELD_NAME]].append(
                                {field[SUB_FIELD][FIELD_NAME]: sub_value})

                    else:
                        section_data[-1][cur_sub_section[FIELD_NAME]].append({field[FIELD_NAME]: value})

                    last_line_id = cur_id + 1
                    exist_flag = True
                    break

        return section_data, last_line_id
