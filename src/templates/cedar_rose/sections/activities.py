from src.parse.parse_utils import *
from copy import deepcopy
from src.annotation.anno_utils import LINE_TEXT


class Activities:
    def __init__(self, info):
        self.info = info

    def extract_data(self, lines, last_line_id):
        section_data = []
        sub_section_data = []
        exist_flag = False

        # check the start line id with sections keywords
        cur_sub_section = None
        cur_id = last_line_id
        while cur_id < len(lines):
            if exist_flag and last_line_id > IN_SAME_GROUP and cur_id - last_line_id > IN_SAME_GROUP:
                break

            line_text = lines[cur_id][LINE_TEXT]

            # check new sub_section start
            is_start_new_sub_section = False
            for sub_section in self.info[SUB_SECTIONS]:
                pos, val = get_obj_value(line_text=line_text, keywords=sub_section[KEYWORDS])
                if pos < len(sub_section[KEYWORDS][0]) and val:
                    is_start_new_sub_section = True
                    cur_sub_section = deepcopy(sub_section)
                    break

            # new sub sections then go to
            if is_start_new_sub_section:
                exist_flag = True
                section_data.append({
                    cur_sub_section[FIELD_NAME]: []
                })
                sub_section_data.clear()

            for field in self.info[FIELDS]:
                find_pos, value = get_obj_value(line_text=line_text, keywords=field[KEYWORDS])
                if value is not None:
                    if is_start_new_sub_section:
                        target = section_data[-1][cur_sub_section[FIELD_NAME]]
                    else:
                        target = section_data

                    if (MULTILINE in field.keys() and field[MULTILINE]) or (
                            ORIENTATION in field.keys() and field[ORIENTATION] == "under"):
                        # check the next lines whether any field key is exist or not
                        is_with_keyword = False
                        multi_ext_lines = 0
                        while not is_with_keyword:
                            _ext_line_text = lines[cur_id + multi_ext_lines + 1][LINE_TEXT]

                            for field_1 in self.info[FIELDS]:
                                _pos, _ = get_obj_value(line_text=_ext_line_text, keywords=field_1[KEYWORDS])
                                if _pos != -1:
                                    is_with_keyword = True
                                    break
                            if not is_with_keyword:
                                value += APPENDER + _ext_line_text
                                multi_ext_lines += 1

                        cur_id += multi_ext_lines

                    if SUB_FIELD in field.keys():
                        sub_find_pos, sub_value = get_obj_value(line_text=value, keywords=field[SUB_FIELD][KEYWORDS],
                                                                sub_field=True)
                        if sub_value is not None:
                            target.append({field[FIELD_NAME]: value[:sub_find_pos]})
                            target.append({field[SUB_FIELD][FIELD_NAME]: sub_value})
                    else:
                        target.append({field[FIELD_NAME]: value})

                    exist_flag = True
                    last_line_id = cur_id + 1
                    break

            cur_id += 1
        return section_data, last_line_id
