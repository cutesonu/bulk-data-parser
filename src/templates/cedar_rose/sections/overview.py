from src.parse.parse_utils import *
from src.annotation.anno_utils import LINE_TEXT


class Overview:
    def __init__(self, info):
        self.info = info

    def extract_data(self, lines, last_line_id):
        section_data = []
        exist_flag = False

        # check the start line id with sections keywords
        for field in self.info[FIELDS]:
            for cur_id in range(last_line_id, len(lines)):
                if exist_flag and cur_id - last_line_id > IN_SAME_GROUP:
                    break

                line_text = lines[cur_id][LINE_TEXT]

                # check sub_section start
                is_start_new_sub_section = False
                for sub_section in self.info[SUB_SECTIONS]:
                    pos, val = get_obj_value(line_text=line_text, keywords=sub_section[KEYWORDS])
                    if pos < len(sub_section[KEYWORDS][0]) and val:
                        is_start_new_sub_section = True
                        break
                if is_start_new_sub_section:
                    exist_flag = True
                    last_line_id += 1
                    break

                # check sections
                value = get_obj_value(line_text=line_text, keywords=field[KEYWORDS])[1]
                if value is not None:
                    key = field[FIELD_NAME]
                    section_data.append({key: value})
                    last_line_id = cur_id + 1
                    exist_flag = True
                    break

        return section_data, last_line_id
