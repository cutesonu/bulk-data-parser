#     PyPDF2
#         :return: a unicode string object.


import PyPDF2
import sys
import src.logger as log


class Pdf2Txt:
    def __init__(self):
        pass

    @staticmethod
    def pdf_to_txt(pdf_path):
        try:
            with open(pdf_path, 'rb') as pdf_fp:
                pdf_reader = PyPDF2.PdfFileReader(pdf_fp)
                num_pages = pdf_reader.numPages
                log.log_info("num pages :{} ".format(num_pages))

                page_contents = []
                for i in range(num_pages):
                    page = pdf_reader.getPage(i)
                    page_contents.append(page.extractText())

                return page_contents

        except Exception as e:
            print(e)
            log.log_error(str(e))
            sys.exit(1)


if __name__ == '__main__':
    pdf = Pdf2Txt()
    path = "/home/suno/Desktop/Bulk-Data-Parser/bulk-data-parser/data/samples/SSC1394201-Company Credit Report.pdf"
    print(pdf.pdf_to_txt(pdf_path=path))
