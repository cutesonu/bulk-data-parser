from xmljson import badgerfish as bf
from xml.etree.ElementTree import fromstring
import src.logger as log
from src.common.text_utils import is_empty_string

from src.static import ANNO_TEXT, ANNO_BBOX, ANNO_VERTICES
from src.static import PAGE_ID, PAGE_CONTENT, PAGE_ROTATE, PAGE_POSITION, PAGE_BBOX, PAGE_ANNOS


TAG_GROUP_START = '<pages'
TAG_GROUP_END = '</pages'
TAG_PAGE_START = '<page'
TAG_PAGE_END = '</page'
TAG_TEXTBOX_START = '<textbox'
TAG_TEXTBOX_END = '</textbox'

KEY_TEXTBOX = 'textbox'
KEY_TEXTLINE = 'textline'
KEY_TEXT = 'text'
KEY_BBOX = '@bbox'
KEY_FONT = '@font'
KEY_SIZE = '@size'
KEY_CHAR = '$'
KEY_ID = '@id'

KEY_ROTATE = 'rotate'
KEY_PAGE_ID = 'page id'
KEY_PAGE_ANNOS = 'page_annos'

SPACE = " "
EMPTY = ""


"""
{"textbox": 
    {"@bbox": "67.584,783.038,310.580,803.725", "@id": 3, 
     "textline": 
        {"@bbox": "67.584,783.038,310.580,803.725", 
         "text":
         [
        {"@bbox": "67.584,783.038,81.024,803.725", "@font": "ABCDEE+Trebuchet MS,Bold", "@size": 20.686, "$": "C"}, 
        {"@bbox": "81.024,783.038,93.453,803.725", "@font": "ABCDEE+Trebuchet MS,Bold", "@size": 20.686, "$": "o"}, 
        {"@bbox": "93.453,783.038,112.317,803.725", "@font": "ABCDEE+Trebuchet MS,Bold", "@size": 20.686, "$": "m"}, 
        {"@bbox": "112.317,783.038,125.119,803.725", "@font": "ABCDEE+Trebuchet MS,Bold", "@size": 20.686, "$": "p"}, 
        {"@bbox": "125.119,783.038,136.824,803.725", "@font": "ABCDEE+Trebuchet MS,Bold", "@size": 20.686, "$": "a"}, 
        {"@bbox": "136.824,783.038,149.780,803.725", "@font": "ABCDEE+Trebuchet MS,Bold", "@size": 20.686, "$": "n"},
        {"@bbox": "149.780,783.038,161.507,803.725", "@font": "ABCDEE+Trebuchet MS,Bold", "@size": 20.686, "$": "y"}, 
        {"@bbox": "161.507,783.038,168.117,803.725", "@font": "ABCDEE+Trebuchet MS,Bold", "@size": 20.686}, 
        {"@bbox": "168.249,783.038,181.688,803.725", "@font": "ABCDEE+Trebuchet MS,Bold", "@size": 20.686, "$": "C"}, 
        {"@bbox": "181.776,783.038,191.153,803.725", "@font": "ABCDEE+Trebuchet MS,Bold", "@size": 20.686, "$": "r"},
        {"@bbox": "191.153,783.038,203.780,803.725", "@font": "ABCDEE+Trebuchet MS,Bold", "@size": 20.686, "$": "e"}, 
        {"@bbox": "203.780,783.038,216.539,803.725", "@font": "ABCDEE+Trebuchet MS,Bold", "@size": 20.686, "$": "d"}, 
        {"@bbox": "216.451,783.038,222.995,803.725", "@font": "ABCDEE+Trebuchet MS,Bold", "@size": 20.686, "$": "i"}, 
        {"@bbox": "222.995,783.038,231.691,803.725", "@font": "ABCDEE+Trebuchet MS,Bold", "@size": 20.686, "$": "t"}, 
        {"@bbox": "231.779,783.038,238.389,803.725", "@font": "ABCDEE+Trebuchet MS,Bold", "@size": 20.686},      
        {"@bbox": "308.090,785.389,310.580,794.442", "@font": "Times New Roman", "@size": 9.054},
        {}
        ]}
    }
}
"""


class ParseXml:
    def __init__(self, debug=False):
        self.debug = debug
        pass

    def parse_xml2anno(self, raw_xml_string):
        pages = self.xml2json(raw_string=raw_xml_string)

        for page in pages:
            page_annos = self.json2annos(page_content=page[PAGE_CONTENT], page_bbox=page[PAGE_BBOX])
            page[PAGE_ANNOS] = page_annos

        return pages

    @staticmethod
    def json2annos(page_content, page_bbox):
        """
            annotation = {
                u'text': u'Description',
                u'boundingBox': {
                    u'vertices': [{u'x': 13, u'y': 8}, {u'x': 13, u'y': 18}, {u'x': 31, u'y': 18}, {u'x': 31, u'y': 8}]
                    }
            }
        """
        _, _, x0, y0 = page_bbox
        """
            pdfminer's O(0, 0)point is left-bottom instead of left-top, so the (y1, y2) should be changed (h-y2, h-y1)
        """
        annos = []
        for textbox in page_content:
            for textline in textbox[KEY_TEXTLINE]:
                bbox = textline[KEY_BBOX]
                text = textline[KEY_TEXT]
                if not is_empty_string(text=text):
                    x1, _y1, x2, _y2 = [float(val) for val in bbox.split(",")]
                    y1 = y0 - _y2
                    y2 = y0 - _y1
                    anno = {
                        ANNO_TEXT: text,
                        ANNO_BBOX: {
                            ANNO_VERTICES: [{u'x': x1, u'y': y1}, {u'x': x2, u'y': y1}, {u'x': x2, u'y': y2},
                                            {u'x': x1, u'y': y2}]
                        }
                    }
                    annos.append(anno)
        return annos

    def xml2json(self, raw_string):
        """
        page: {
            page_id :
            page_rotate :
            page_bbox :
            position :

            page_content : (list of textboxes)
                [
                    {
                        KEY_ID :
                        KEY_TEXTLINE: (list of textlines)
                            [
                                {
                                    KEY_BBOX : ,
                                    KEY_TEXT :
                                },
                                ...
                            ]
                    },
                    ...
                ]
        }
        :param raw_string:
        :return:
        """
        _start = raw_string.find(TAG_GROUP_START)
        _end = raw_string.find(TAG_GROUP_END)
        raw_string = raw_string[_start + len(TAG_GROUP_START): _end]
        lines = [line for line in raw_string.splitlines() if len(line) != 0]

        pages = []
        page_id = 0

        pos_page_start = -1
        pos_page_end = -1
        cur_pos = 0

        log.log_info("\t seek the start/end pos of each page")
        while cur_pos < len(lines):
            # seek the start/end of each page
            for i in range(cur_pos, len(lines)):
                line = lines[i]
                if line.find(TAG_PAGE_START) != -1:
                    pos_page_start = i
                elif line.find(TAG_PAGE_END) != -1 and pos_page_start != -1:
                    pos_page_end = i
                    break

            # sections the page contents
            if pos_page_start != -1 and pos_page_end != -1:
                page_id, page_bbox, rotate = self.__parse_page_header(lines[pos_page_start], idx=page_id)
                pages.append({
                    PAGE_ID: page_id,
                    PAGE_ROTATE: rotate,
                    PAGE_BBOX: page_bbox,
                    PAGE_POSITION: [pos_page_start, pos_page_end]
                })
                page_id += 1
                cur_pos = pos_page_end

                pos_page_start = -1
                pos_page_end = -1
            else:
                break

        log.log_info("\t convert to JSON format")
        for i, page in enumerate(pages):
            _start, _end = pages[i][PAGE_POSITION]
            _page_id = pages[i][PAGE_ID]
            log.log_info("\t  page_id: {}, start: {},  end: {}".format(_page_id, _start, _end))

            page_content = self.__parse_page(raw_page_lines=lines[_start: _end])
            log.log_info("\t     #contents: {}".format(len(page_content)))
            pages[i][PAGE_CONTENT] = page_content

        return pages

    def __parse_page(self, raw_page_lines):
        """
        page_content : list of textbox

        """
        textboxes = []

        pos_textbox_start = -1
        pos_textbox_end = -1
        for j, line in enumerate(raw_page_lines):
            if line.find(TAG_TEXTBOX_START) != -1:
                pos_textbox_start = j
            elif line.find(TAG_TEXTBOX_END) != -1 and pos_textbox_start != -1:
                pos_textbox_end = j

            if pos_textbox_start != -1 and pos_textbox_end != -1 and pos_textbox_start < pos_textbox_end:

                textbox = self.__parse_textbox(raw_textbox_lines=raw_page_lines[pos_textbox_start: pos_textbox_end + 1])

                if textbox[KEY_ID] == -1:
                    continue
                else:
                    textboxes.append(textbox)

                pos_textbox_start = -1
                pos_textbox_end = -1

        return textboxes

    def __parse_textbox(self, raw_textbox_lines):
        """
        textbox : {
            KEY_ID : ,
            KEY_TEXTLINE : [] (list of textline)
        }

        """

        textbox_string = ""

        for i, line in enumerate(raw_textbox_lines):
            textbox_string += line

        textbox_id = -1
        textlines = []

        try:
            ordered_dict = bf.data(fromstring(textbox_string))
            textbox_dict = ordered_dict[KEY_TEXTBOX]

            textbox_id = textbox_dict[KEY_ID]  # textbox.id

            textline_dictorlist = textbox_dict[KEY_TEXTLINE]
            if type(textline_dictorlist) == list:
                for sub_dict in textline_dictorlist:
                    line_bbox, line_text = self.__parse_textline(textline_dict=sub_dict)
                    if line_text is not None:
                        textlines.append({KEY_BBOX: line_bbox, KEY_TEXT: line_text})
            else:
                line_bbox, line_text = self.__parse_textline(textline_dict=textline_dictorlist)
                if line_text is not None:
                    textlines.append({KEY_BBOX: line_bbox, KEY_TEXT: line_text})

        except Exception as e:
            print("\t" + str(e))
            log.log_warn("\t issue on textbox id: {}".format(textbox_id))

        if self.debug:
            print("textbox id: {}".format(textbox_id))
            for line_text in textlines:
                print("\t {}".format(line_text))

        return {KEY_ID: textbox_id,
                KEY_TEXTLINE: textlines}

    @staticmethod
    def __parse_textline(textline_dict):
        """
        textline : {
            KEY_BBOX : ,
            KEY_TEXT :
        }

        """
        textline_bbox = ""
        textline_string = ""

        try:
            textline_bbox = textline_dict[KEY_BBOX]  # textline.bbox
            text_dictorlist = textline_dict[KEY_TEXT]  # textline.text
            if type(text_dictorlist) == list:
                for sub_dict in text_dictorlist:
                    try:
                        textline_string += str(sub_dict[KEY_CHAR])
                    except KeyError:
                        textline_string += SPACE
            else:
                textline_string = text_dictorlist[KEY_CHAR]

        except Exception as e:
            print(e)
            pass

        if is_empty_string(text=textline_string):
            textline_string = None
        return textline_bbox, textline_string

    @staticmethod
    def __parse_page_header(page_header_string, idx):
        """ ex:
            '<page id="1" bbox="0.000,0.000,601.800,878.640" rotate="0">'
        """
        page_id = -1
        bbox = [0, 0, 1000, 1000]
        rotate = 0

        try:
            if page_header_string.find(KEY_PAGE_ID) != -1:
                _start_pos = page_header_string.find(KEY_PAGE_ID) + len(KEY_PAGE_ID)
                _end_pos = page_header_string.find(SPACE, _start_pos)
                page_id = int(page_header_string[_start_pos + 1: _end_pos].replace("=", EMPTY).replace('"', EMPTY)) - 1
            else:
                page_id = idx
                _end_pos = 0

            new_string = page_header_string[_end_pos:]
            for sp in new_string.split(SPACE):
                if sp.find(KEY_ROTATE) != -1:
                    _start_pos = sp.find(KEY_ROTATE) + len(KEY_ROTATE) + 1
                    _end_pos = sp.rfind('"', _start_pos)
                    rotate = int(sp[_start_pos:_end_pos].replace('"', EMPTY))
                elif sp.find(KEY_BBOX[1:]) != -1 or sp.find('bbox') != -1:
                    _start_pos = sp.find(KEY_BBOX) + len(KEY_BBOX) + 1
                    _end_pos = sp.rfind('"', _start_pos)
                    bbox = [float(val) for val in sp[_start_pos:_end_pos].replace('"', EMPTY).split(",")]
                else:
                    continue

        except Exception as e:
            print(e)

        return page_id, bbox, rotate
