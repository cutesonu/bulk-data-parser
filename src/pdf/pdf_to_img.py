import os
import sys
import src.logger as log


class Pdf2Img:

    def __init__(self, debug=False):
        self.debug = debug

    def pdf_to_imgs(self, pdf_path):

        if not os.path.isfile(pdf_path):
            log.log_error("\tno exist such pdf file {}".format(pdf_path))
            sys.exit(1)

        _, ext = os.path.splitext(os.path.basename(pdf_path))
        file_type = ext[1:].upper()

        if file_type in ["PDF"]:
            page_imgs = self.__pdf2imgs_ppm(pdf_path)
            page_imgs.sort()
            return page_imgs
        else:  # not yet
            log.log_error("\tnot defined file type.")
            sys.exit(1)

    @staticmethod
    def __pdf2imgs_ppm(_pdf_path):

        # get the base name for the converted jpg image files
        parent, fname = os.path.split(_pdf_path)
        base, ext = os.path.splitext(fname)
        out_base, _ = os.path.splitext(_pdf_path)

        # convert the pdf file to the jpg images
        command = 'pdftoppm %s %s -jpeg' % (_pdf_path.replace(' ', '\ '), out_base.replace(' ', '\ '))
        os.system(command)

        paths = []
        # convert the jpg files to the list of cv image
        for f in os.listdir(parent):
            path = os.path.join(parent, f)
            if os.path.exists(path) and f.find(base) != -1 and os.path.splitext(f)[1].find('jpg') != -1:
                paths.append(path)

        return paths


if __name__ == '__main__':
    import os
    from src.settings import ROOT_DIR
    sample_path = os.path.join(ROOT_DIR, "data/samples/SSC1394201-Company Credit Report.pdf")
    pdf = Pdf2Img()
    page_paths = pdf.pdf_to_imgs(pdf_path=sample_path)
    print(page_paths)
