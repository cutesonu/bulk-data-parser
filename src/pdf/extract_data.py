#     pdfminer
#       if python3:
#             pdfminer.six
#       if python2:
#             pdfminer
#         :return: a unicode string, html, xml


import io
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import TextConverter, HTMLConverter, XMLConverter
from pdfminer.layout import LAParams

import sys
import src.logger as log


class Pdf2Data:
    def __init__(self, codec='utf-8', cvt_format='xml'):
        self.rsrcmgr = PDFResourceManager()
        self.retstr = io.BytesIO()
        self.laparams = LAParams()
        if cvt_format == 'text':
            self.device = TextConverter(rsrcmgr=self.rsrcmgr, outfp=self.retstr, codec=codec, laparams=self.laparams)
        elif cvt_format == 'xml':
            self.device = XMLConverter(rsrcmgr=self.rsrcmgr, outfp=self.retstr, codec=codec, laparams=self.laparams)
        elif cvt_format == 'html':
            self.device = HTMLConverter(rsrcmgr=self.rsrcmgr, outfp=self.retstr, codec=codec, laparams=self.laparams)
        else:
            log.log_error('provided format, either text, html or xml')
            sys.exit(1)
            # raise ValueError('provided format, either text, html or xml')

    def pdf_to_anno_data(self, pdf_path, password=''):
        text = None
        try:
            interpreter = PDFPageInterpreter(self.rsrcmgr, self.device)
            with open(pdf_path, 'rb') as pdf_fp:
                maxpages = 0
                caching = True
                pagenos = set()

                for page in PDFPage.get_pages(pdf_fp, pagenos=pagenos, maxpages=maxpages, password=password,
                                              caching=caching, check_extractable=True):
                    interpreter.process_page(page)
            text = self.retstr.getvalue().decode()
        except Exception as e:
            log.log_warn(str(e))

        return text

    def release(self):
        self.device.close()
        self.retstr.close()


if __name__ == '__main__':
    pdf = Pdf2Data()
    # path = "/home/suno/Desktop/Bulk-Data-Parser/bulk-data-parser/data/samples/SSC1394201-Company Credit Report.pdf"
    path = "/home/suno/Desktop/Bulk-Data-Parser/bulk-data-parser/data/AEC1124207-Company Credit Report.pdf"
    raw = pdf.pdf_to_anno_data(pdf_path=path)
