#!/usr/bin/env bash
# [tesseract_install.sh]

cd /tmp

# install the tesseract
sudo apt-get-repository ppa:alex-p/tesseract-ocr
sudo apt-get update
sudo apt install tesseract-ocr
sudo apt install libtesseract-dev
sudo pip install pytesseract

# check version
tesseract --version
