numpy
xmljson
Pillow==3.1.2
fuzzywuzzy==0.17.0
pytesseract==0.2.5
opencv-contrib-python==3.4.3
opencv-python

pyPDF2==1.26.0
pdfminer.six
